const path = require('path');
const dataObj = require("./data/data.json");

module.exports = {
  runtimeCompiler: true,
  css: {
    requireModuleExtension: false,
    loaderOptions: {
      scss: {
        prependData: `@import "~@/scss/main.scss";`,
      },
    },
  },
  configureWebpack: {
    entry: [
      './src/app.js',
    ],
    module: {
      rules: [
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          use: {
            loader: 'url-loader',
            options: {
              name: '[name].[ext]',
            },
          },
        },
      ],
    },
    resolve: {
      alias: {
        vue$: path.resolve(__dirname, 'node_modules/vue/dist/vue.esm.js'),
        fonts : path.resolve(__dirname, 'src/assets/fonts'),
        '@': path.resolve(__dirname, 'src'),
      },
    },
  },
	devServer: {
		contentBase: path.join(__dirname, 'public'),
    port: 9000,
    historyApiFallback: true,
		before: function(app, server, compiler) {
			app.get('/api/categories', function (req, res) {
				res.json(dataObj.categories);
			});

			app.get('/api/category/*', function (req, res) {
				res.json(dataObj.articles);
			});

			app.get('/api/author/*', function (req, res) {
				let author = {};
				const authorId = req.params['0'];

				for (let index = 0; index < dataObj.authors.length; index++) {
					if (dataObj.authors[index].id === authorId) {
						author = dataObj.authors[index];
						break;
					}
					
				}
				res.json(author);
			});

			app.get('/api/search/*', function (req, res) {
				res.json(dataObj.articles);
			});
		}
  },
};
