import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

// ========================
//    BUILD ENTRY ROUTES
// ========================

const routes = [
  {
    path: '/',
    name: 'main',
    component: require('../pages/Main.vue').default,
  },
  {
    path: '/category-details/:id',
    name: 'category-details',
    component: require('../pages/Category.vue').default,
    meta: {
      breadcrumbs: [
        {
          text: 'All Categories',
          name: 'main',
        },
        {
          text: 'Category Details',
          active: true,
        },
      ],
    },
  },
  {
    path: '/search-articles',
    name: 'search-articles',
    component: require('../pages/SearchArticle.vue').default,
    meta: {
      breadcrumbs: [
        {
          text: 'All Categories',
          name: 'main',
        },
        {
          text: 'Search Results',
          active: true,
        },
      ],
    },
  }
];

// ========================
//      BUILD ROUTER
// ========================

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
