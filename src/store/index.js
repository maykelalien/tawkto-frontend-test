import Vue from 'vue';
import Vuex from 'vuex';

import _ from 'lodash';
import Axios from 'axios';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    categoriesSearchText: null,
    categories: null,
    categoryDetails: null,
    searchedArticles: null,
  },
  mutations: {
    updateCategoriesSearchText (state, value) {
      state.categoriesSearchText = value;
    },
    updateCategories (state, value) {
      state.categories = value;
    },
    updateCategoryDetails (state, value) {
      state.categoryDetails = value;
    },
    updateSearchedArticles (state, value) {
      state.searchedArticles = value;
    },
  },
  actions: {
    async loadCategories ({ commit }, { searchString, enabled }) {
      const items = await Axios
        .get(`http://localhost:9000/api/categories`)
        .then(result => {
          const items = result
            ?.data
            ?.filter(({ title, description }) => {
              if (!searchString?.trim()?.length) return true;
              if (!!title?.match(new RegExp(searchString, 'i'))) return true;
              if (!!description?.match(new RegExp(searchString, 'i'))) return true;
              return false;
            })
            ?.filter(item => !enabled || item.enabled);
          return _.sortBy(items, ['order', 'title']);
        });

      commit('updateCategories', items);
    },
    async loadCategoryDetails ({ commit }, { id }) {
      if (!id) throw new Error('No id provide on loading category.')
      const category = await Axios
        .get(`http://localhost:9000/api/categories`)
        .then(result => result?.data?.find(item => item.id === id));
      if (!category?.id) throw new Error('Category doesn\'t exist');

      const articles = await Axios
        .get(`http://localhost:9000/api/category/${id}`)
        .then(result => result?.data?.filter(item => item.status === 'published'));

      const result = { ...category, $populated: { articles } };
      commit('updateCategoryDetails', result);
    },
    clearCategoryDetails () {
      commit('updateCategoryDetails', null);
    },
    async loadOtherCategories (context, { excludedId, page = 1, enabled}) {
      const items = await Axios
        .get(`http://localhost:9000/api/categories`)
        .then(result => {
          const items = result
            ?.data
            ?.filter(({ id }) => excludedId !== id)
            ?.filter(item => !enabled || item.enabled);
          return _.sortBy(items, ['order', 'title']);
        });
      const end = page * 3;
      const start = end - 3;
      return items?.slice(start, end);
    },
    async searchArticles ({ commit }, { searchString }) {
      if (!searchString?.trim()?.length) {
        commit('updateSearchedArticles', []);
        return;
      }
      
      console.log('searchString', searchString);
      const items = await Axios
        .get(`http://localhost:9000/api/search/${searchString}`)
        .then(result => result?.data
          ?.filter(({ title }) => !!title?.match(new RegExp(searchString, 'i'))));

      console.log('items', items);
      commit('updateSearchedArticles', items);
    },
  },
});

export default store;